# Contributing #
Generally just test the adjustments you've made. Platform specific things are
listed below.


# Windows #
- **Open** the file in MSKLC and **save** it\
   This ensures that general formatting rules defined by the software are met.
   If you want to do everyone a favor then **add some tabs** to fix some of the
   weird spacing this causes.
- **Build** the layout\
   This ensures that it still works

