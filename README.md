# Katrin Multilingual Dvorak #
<sup>Or KMD for short</sup>

My objective in creating this layout is quite simple: Combine the (highly
subjecive) nice feel of Dvorak with the conveniences US-QWERTY offers for coding
and <abbr title="Canadian Multilingual Standard">CMS</abbr>'s ability to type
(almost) any language that's based on the latin script.

I'm aware that there are more layouts besides
<abbr title="Canadian Multilingual Standard">CMS</abbr> that can type various
latin-based scripts (such as [*Horne's international Dvorak latin*][Horne Dvorak latin])
but that was my primary inspiration.

1. [Modifying the Layout](#modifying-the-layout)
2. [Contributing](#contributing)
3. [Bibliography](#bibliography)


## Modifying the Layout ##
If you have no experience editing layouts then the READMEs for each platform
should be helpful.


## Contributing ##
Contributions are highly appreciated!

See [CONTRIBUTING.md](CONTRIBUTING.md)


## Bibliography ##
Venturin, Damiano, *A simple, humble but compehensive guide to XKB for linux*, Medium, 13 Jun 2016, https://medium.com/@damko/a-simple-but-comprehensive-guide-to-xkb-for-linux-6f1ad5e13450

[Accents that exist in Unicode](https://www.ncbi.nlm.nih.gov/staff/beck/charents/accents.html)

[Windows' virtual keys](https://docs.microsoft.com/en-us/windows/win32/inputdev/virtual-key-codes)

*XKB, briefely*, The Wayland Protocol, https://wayland-book.com/seat/xkb.html


[Horne Dvorak latin]: https://www.hornetranslations.com/layoutdvorakenca.shtml

