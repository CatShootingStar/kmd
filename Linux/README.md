# Linux keymaps #
Linux uses various kinds of keymaps. So far I support the bare-bones shell
and X, which should cover most people requirements.


## Terminal Keymap ##
If you want to modify this keymap I reccomend having a look at the man
page for `keymaps(5)`.

One notable change you might want to make is setting keycode 58 to
`Caps_Lock`. It would normally be mapped to that but I have mapped it to
`Escape`.


## X Keyboard ##
<sup>or XKB for short.</sup>

As always, if you want to get into the nitty gritty or just started then
[the arch wiki][Arch XKB editing] has you covered.\
Using `xkbcomp` is useful for experimentation. Loading a layout will cause
warnings to appear, but they are not errors so *all is well*✨. To use it for
testing modifications do `$ xkbcomp $DISPLAY keymap.xkb`. Then replate the its
`xkb_symbols` section with your modified `kmd.xkb`. Then do `$ xkbcomp
keymap.xkb $DISPLAY`.

`/usr/include/X11/keysymdef.h` is usefull as it names every charecter defined by
the XKB extension. For any characters not listed here you will need to use
Unicode codepoints.

[Arch XKB editing]: https://wiki.archlinux.org/title/X_keyboard_extension#Editing_the_layout

